<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// generate message
function generateMessage($success, $msg = '', $title = '', $type = '') {
    $arrType = array('error', 'question', 'info', 'warning');

    return array('success'=>$success, 'alertTitle'=>$title, 'alertMsg'=>$msg, 'alertType'=>$type);
}

// ubah format tgl program menjadi format Y-m-d
function dateParser($tgl){
    if ($tgl === NULL)
        return NULL;

    // dapatkan session format tanggal
    $CI =& get_instance();
    $CI->load->library('session');

    $format = $CI->session->userdata('formatTgl') ?? null;

    if ($format == '' || is_null($format))
        return $tgl;

    $pemisah = str_ireplace(array('d', 'm', 'n', 'y'), '', $format);

    $ss = explode($pemisah[0], $tgl);

    $f = explode($pemisah[0], $format);

    // cari posisi tahun
    $posisiY = array_search('Y', $f);
    $y = $ss[$posisiY];

    // cari posisi bulan
    $posisiM = array_search('N', $f);
    if (!$posisiM) {
        $posisiM = array_search('m', $f);
        if (!$posisiM) {
            $posisiM = array_search('M', $f);
        }

        $m = $ss[$posisiM];
    } else {
        $bulan = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];

        $m = array_search($ss[$posisiM], BULAN) + 1;
    }

    // cari posisi hari
    $posisiD = array_search('D', $f);
    if (!$posisiD) {
        $posisiD = array_search('d', $f);
    }
    $d = $ss[$posisiD];

    return date('Y-m-d', strtotime($y.'-'.$m. '-'.$d));
}
// ubah format dari y-m-d ke format program
function formatDate($tgl) {
    //$bulan = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];

    if ($tgl === NULL)
        return NULL;

    // dapatkan session format tanggal
    $CI =& get_instance();
    $CI->load->library('session');

    $format = $CI->session->userdata('formatTgl') ?? null;

    if ($format == '' || is_null($format))
        return $tgl;

    $y = date('Y', strtotime($tgl));
    $m = date('m', strtotime($tgl));
    $d = date('d', strtotime($tgl));

    // replace tahun
    $format = str_replace('Y', $y, $format);

    // replace bulan
    if (strpos($format, 'N') > -1) {
        $format = str_replace('N', BULAN[(int) $m-1], $format);
    } else {
        if (strpos($format, 'M') > -1) {
            $format = str_replace('M', $m, $format);
        } else {
            $format = str_replace('m', (int) $m, $format);
        }
    }

    // replace hari
    if (strpos($format, 'D') > -1) {
        $format = str_replace('D', $d, $format);
    } else {
        $format = str_replace('d', (int) $d, $format);
    }

    return $format;
}
function array_filterku($my_array,$filter,$allow=true) {
    $filtered = array_filter(
        $my_array,
        function ($key) use ($filter,$allow) {
            $ada = in_array($key, $filter);
            return $allow ? $ada : !$ada;
        },
        ARRAY_FILTER_USE_KEY
    );

    return $filtered;
}


function roundAmount($amount){
    // dapatkan session format amount
    // digunakan utk memfilter data numeric sesuai dengan presisi

    $CI =& get_instance();
    $CI->load->library('session');

    $precision = $CI->session->userdata('precisionAmount') ?? 2;

    return round($amount, $precision);
}

function formatAmount($amount){
    // digunakan untuk memformat amount dalam bentuk text/string
    // dapat digunakan dalam menampilkan pesan error atau di laporan

    $CI =& get_instance();
    $CI->load->library('session');

    return number_format($amount, $CI->session->userdata('precisionAmount'), $CI->session->userdata('decimalAmount'), $CI->session->userdata('groupAmount'));
}

function cekTransaksi($id, $tgl){
    /*tambah
        id null, tgl ada
    ubah
        id ada, tgl ada
    hapus
        id ada, tgl null*/
}

// digunakan ketika pembentukan laporan
function generateFilterDate($post){
    if ($post['periode'] == 'bulan') {
        $tglAwal = date('Y-m-d', strtotime($post['tahun_awal'].'-'.$post['bulan_awal'].'-01'));
        $tglAkhir = date('Y-m-t', strtotime($post['tahun_akhir'].'-'.$post['bulan_akhir'].'-01'));

        if ($post['tahun_awal'] == $post['tahun_akhir']) {
            if ($post['bulan_awal'] == $post['bulan_akhir']) {
                $periode = BULAN_FULL[$post['bulan_akhir']-1].' '.$post['tahun_awal'];
            } else {
                $periode = BULAN_FULL[$post['bulan_awal']-1].' - '.BULAN_FULL[$post['bulan_akhir']-1].' '.$post['tahun_awal'];
            }
        } else {
            $periode = BULAN_FULL[$post['bulan_awal']-1].' '.$post['tahun_awal'].' - '.BULAN_FULL[$post['bulan_akhir']-1].' '.$post['tahun_akhir'];
        }
    } else {
        $tglAwal = dateParser($post['tgl_awal']);
        $tglAkhir = dateParser($post['tgl_akhir']);

        $periode = $post['tgl_awal'].' - '.$post['tgl_akhir'];
    }

    return array('awal'=>$tglAwal, 'akhir'=>$tglAkhir, 'periode'=>$periode);
}
