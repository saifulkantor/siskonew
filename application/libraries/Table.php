<?php
class CI_Table {

	private $tbl_str = '';
	private $tbl_width = 0;
	private $tbl_tr = '';
	private $tbl_td = '';
	private $hr;
	private $hr_str = '';
	private $tbl_colspan = 0;
	private $border = 1;
	
	function set_border($val = 1) {
		$this->border = $val;
	}
	
	function set_hr($val = false) {
		$this->hr = $val;
	}
	
	function set_th ($data_td = array()) {
		$this->tbl_width = 0;
		$count_td = count($data_td);
		$temp_count = 0;
		foreach ($data_td as $item) {
			foreach ($item as $row => $values) {
				if ($row=='colspan')
					$temp_count += $values-1;
			
				if ($row=='width') {
					$this->tbl_width += $values;
					break;
				}
			}
		}
		$this->tbl_colspan = $count_td + $temp_count;
		
		$this->set_td($data_td);
	}
	
	function set_td ($data_td = array()) {
		foreach ($data_td as $item) {
			$prop_td = '';
			$value_td = '';
			
			foreach ($item as $row => $values) {
				if ($row!='' and $row!='values')
					$prop_td .= $row.'=\''.$values.'\' ';

				if ($row=='values')  {
					$value_td .= $values;
				}
			}
			
			$this->tbl_td .= '<td '.$prop_td.'>'.$value_td.'</td>';
		}
	}
	function set_tr ($data_tr = array()) {
		if ($this->tbl_tr!='') {
			$this->tbl_str .= $this->tbl_tr.$this->tbl_td.'</tr>';
			$this->tbl_tr = '';
			$this->tbl_td = '';
		}
		
		$this->tbl_tr .= '<tr ';
		foreach ($data_tr as $row => $values) {
			if ($row!='')
				$this->tbl_tr .= $row.'=\''.$values.'\' ';
		}
		$this->tbl_tr .= '>';
	}
	function line_break($colspan = 0) {
		$this->set_tr();
		$this->set_td(array(array('style'=>'border-right:#FFF 1px solid; border-left:#FFF 1px solid;', 'colspan'=>$this->tbl_colspan, 'values' => '&nbsp;')));
	}
	
	function generate() {
		if ($this->tbl_tr!='') {
			$this->tbl_str .= $this->tbl_tr.$this->tbl_td.'</tr>';
			$this->tbl_tr = '';
			$this->tbl_td = '';
		}
		
		if ($this->hr) {
			$this->hr_str = '<hr style="border:0; background-color:black; height:1px; width:'.$this->tbl_width.'px" align="left"/>';
		}

		$gen = $this->hr_str.'<table class="table-report" style="width:'.$this->tbl_width.'px; border-collapse:collapse;" border="'.$this->border.'">'.$this->tbl_str.'</table>';

		// reset
		$this->tbl_str = $this->tbl_tr = $this->tbl_td = $this->hr_str = '';
		$this->tbl_width = 0;

		return $gen;
	}
}