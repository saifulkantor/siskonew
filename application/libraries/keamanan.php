<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class keamanan {
  public $usermenu = null;
  public $linkaman = array('login','logout','notfound','notallowed','admin5427','');
  public $allowedaction = array('tambah'=>false,'ubah'=>false,'hapus'=>false);
 function generatecsrf($lokasi='index')
 {
  $key='usDJKEtd$#8hftqm4ij';
  return sha1($lokasi.$key);
 }
 function generatepassword($pass='')
 {
  $key='kjk&84x4m^&#$sjKDWakfg';
  return sha1($pass.$key);
 }
 function generateauth_key($id_user=0)
 {
  $rand=rand(0, 999);
  $key='o3qhyfsfdasfstgfwe4tq';
  return sha1($rand.$key.$id_user);
 }
 function generateid_device($id_device=0)
 {
  $key='23oH&v8fk67ePr^K%';
  return sha1($key.$id_device);
 }
 function checkrule($slug){
   $arr = array('tambah','ubah','hapus');
   $controller=(strpos($slug, '/')>0)?substr($slug, 0, (strpos($slug, '/'))):$slug;
   $ext=pathinfo($slug, PATHINFO_EXTENSION);
   $izin = false;
   if (in_array($controller,$this->linkaman)||$slug=='perusahaan/listperusahaan'||strpos($slug, 'erusahaan/gantiperusahaan')||in_array($ext,array('jpg','png'))) {
      $izin = true;
   }
   foreach ($this->usermenu as $key2 => $menu) {
     if ($menu['slug']==$slug || $menu['slug']==$controller) {
       $izin = true;
       foreach ($arr as $key => $value) {
         $this->allowedaction[$value] = $menu['act_'.$value];
         if (strpos($slug, $value)&&$menu['act_'.$value]==0) {
            $izin = false;
         }
       }
     }
   }
   return $izin;
 }

}
/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */
