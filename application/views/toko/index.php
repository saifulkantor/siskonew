<div class="utamabox">
  <div class="kategoribox">
  <div class="col col-xs-12" style="overflow:hidden;margin-bottom: 0px;margin-top: 5px;">
    <div class="row">
      <form id="formfilter" onsubmit="return mySubmitFunction(event)" style="width:100%">
        <div class="form-row" style="padding-left: 4px;padding-right: 4px;">
          <div class="col col-sm-7">
            <div class="form-group mb-2">
              <input type="hidden" class="kat" name="kat" value="<?=$get['kat']??''?>">
              <label for="cariproduk" class="sr-only">Cari Produk</label>
              <div class="input-group mb-2">
                <input type="text" class="form-control" name="nama" id="cariproduk" placeholder="Cari Produk" value="<?=$get['nama']??''?>">
                <div class="input-group-append" onclick="filterchange()" style="cursor:pointer">
                  <div class="input-group-text">Cari</div>
                </div>
              </div>
            </div>
          </div>
          <div class="col col-sm-5">
    					<select id="urutkan" name="urutkan" class="form-control" onchange="filterchange()" style="width:100%">
    						<option value="">-- Default --</option>
    						<option <?=(($get['urutkan']??'')=='harga asc')?'selected':''?> value="harga asc">Harga Termurah</option>
    						<option <?=(($get['urutkan']??'')=='harga desc')?'selected':''?> value="harga desc">Harga Termahal</option>
    						<option <?=(($get['urutkan']??'')=='nama asc')?'selected':''?> value="nama asc">Nama Komoditas A-Z</option>
    						<option <?=(($get['urutkan']??'')=='nama desc')?'selected':''?> value="nama desc">Nama Komoditas Z-A</option>
    					</select>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="col col-xs-12" style="overflow:auto;margin-bottom: 5px;">
    <div class="row" style="width: max-content;">
    <?php $katdipilih = $get['kat']??'';
    echo $this->Komoditas->tampilankategori(['id'=>'semua','nama'=>'Semua Produk'],($katdipilih=='' || $katdipilih=='semua')?true:false);
      foreach ($listkategori as $key => $kategori) {
          echo $this->Komoditas->tampilankategori($kategori,($katdipilih==$kategori['id'])?true:false);
        } ?>
    </div>
  </div>
</div>
  <!-- Main content -->
  <div class="col col-xs-12 datakomoditas" style="margin-top: 194px;">
    <div class="row">
    <?php /* foreach ($listkomoditas as $key => $komoditas) {
          echo $this->Komoditas->tampilantable($komoditas);
        } ?>
        <div class="loadmore" >
          <button type="button" class="btn btn-primary" onclick="getkomoditas(2)">Load More</button>
        </div> */ ?>
    </div>
  </div>
</div>
<div class="toko cart">
    <h5>Keranjang Belanja <span class="totalpesanan">(0)</span></h5>
  <div class="box">
    <div style="padding:5px;height:calc(100vh - 220px);overflow: auto;">
      <table class="table table-sm" style="font-size: 11px;">
        <tbody>
            <tr>
              <td class="empty-cart">
                Belum ada produk
              </td>
            </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="subtotal">Subtotal: <b>Rp 0</b></div>
  <div class="level-item">
    <button class="btn btn-primary" type="button">Checkout</button>
  </div>
</div>
<script type="text/javascript">
  var page = 1;
  var katdipilih = '<?=$katdipilih?>';
  var listkomoditas = Array();
  var listkeranjang = <?=json_encode($_SESSION['keranjang']??'')?>;
  function imageloader() {
    var html='';
    var table='<div class="col-6 col-sm-4 col-md-3" style="padding-left:7px;padding-right:7px;"><div class="peritem" onclick=""><div class="productload" style="width:100%;height: 180px;"></div><div class="productload" style="width:80%;height:20px;margin:10px auto;"></div><div class="productload" style="width:80%;height:20px;margin:10px auto;"></div></div></div>';
    for (i=1;i<24;i++) {
      html+=table;
    }
    return html;
  }
  function getkomoditas(pg=0) {
    var sct = 0;
    if (pg>1) $('.datakomoditas>.row>.loadmore').html(imageloader()); else $('.datakomoditas>.row').html(imageloader());
    if (pg>1) sct=$('.loadmore').position().top;
    $('html, body').animate({scrollTop : sct},500);
    var request = $.ajax({
      url: "<?=BASE_URL?>ajax/komoditas?page="+pg+"&"+$('#formfilter').serialize(),
      method: "POST",
      data: { csrf : '<?=$this->keamanan->generatecsrf()?>' },
      dataType: "json"
    });

    request.done(function( datahasil ) {
      listkomoditas=datahasil;
      var table = '';
      var perpage = <?=$get['perpage']?>;
      var jmlkom=0;
      $.each(datahasil,function(key,value){
        table+='<?= $this->Komoditas->tampilantable(['id'=>0,'nama'=>0,'deskripsi'=>0,'harga'=>0,'satuan'=>0,'stok'=>0],true)?>';
        jmlkom++;
      });
      if (pg>1) $('.datakomoditas>.row>.loadmore').remove(); else $('.datakomoditas>.row').html('');
      if (jmlkom>=perpage) table+='<div class="loadmore"> <button type="button" class="btn btn-primary" onclick="getkomoditas('+(pg+1)+')">Load More</button></div>';
      $('.datakomoditas>.row').append(table);
      page=pg;
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
    });
  }
  function gantikategori(id_kat='') {
    $('#formfilter #cariproduk').val('');
    $('#formfilter .kat').val(id_kat);
    katdipilih=id_kat;
    $('.perkategori.aktif').removeClass('aktif');
    $('.perkategori'+katdipilih).addClass('aktif');
    gantiurlbar('Sisko','<?=BASE_URL?>toko?'+$('#formfilter').serialize());
    getkomoditas(1);
    // $('#formfilter').submit();
  }

  function filterchange() {
    gantiurlbar('Sisko','<?=BASE_URL?>toko?'+$('#formfilter').serialize());
    getkomoditas(1);
  }

  function addtocartajax(id=0,jumlah=1) {
    // $('.toko.cart table>tbody').html('');
    tampilprosesloading(true);
    var request = $.ajax({
      url: "<?=BASE_URL?>ajax/addtocart",
      method: "POST",
      data: { csrf : '<?=$this->keamanan->generatecsrf()?>', id : id, jumlah : jumlah },
      dataType: "json"
    });

    request.done(function( datahasil ) {
      if (typeof datahasil['komoditas'] === 'undefined') {
        listkeranjang=datahasil;
      } else {
        alert(datahasil['komoditas']);
      }
      updatekeranjang();
      tampilprosesloading(false);
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      updatekeranjang();
      tampilprosesloading(false);
    });
  }

  function addtocart(id_kom=0) {
    var carikom = listkomoditas.findIndex(function(kom) { return kom['id']==id_kom && kom['stok']>0; });
    if (carikom>-1) {
      addtocartajax(id_kom,1);
    } else {
      alert('Produk Tidak ditemukan');
    }
  }

  function updatekeranjang() {
    $('.totalpesanan').html('('+(Object.keys(listkeranjang).length)+')');
    var html=''; var subtotal=0;
    $.each(listkeranjang,function(key,value){
      var harganya = value['harga']*value['jumlah'];
      html += '<tr style="background: white;"><td class="cart'+value['id']+'" style="width: 27px;padding: 2px;">'+
      '<img src="<?=base_url()?>assets/images/komoditas/'+value['id']+'/image1.jpg" class="img-responsive" style="width:100%;max-width: 25px;">'+
      '</td><td>'+value['nama']+'<br/><input class="icart'+value['id']+'" type="number" value="'+value['jumlah']+'" min="1" onchange="addtocartajax('+value['id']+',parseInt($(this).val())-'+value['jumlah']+')"> '+value['satuan']+'</td>'+
      '<td style="width:1%;white-space:nowrap;text-align:right"><br/>'+harganya.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td></tr>';
      subtotal+=parseFloat(harganya);
    });
    $('.toko.cart table>tbody').html(html);
    $('.toko.cart .subtotal b').html('Rp. '+subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
  }

  function tampilprosesloading(tampil=false) {
    if (tampil) $('body').append('<div class="prosesloading"><div class="loader"></div></div>'); else $('.prosesloading').remove();
  }

  document.addEventListener('DOMContentLoaded', (event) => {
    getkomoditas(1);
    updatekeranjang();
  });

</script>
