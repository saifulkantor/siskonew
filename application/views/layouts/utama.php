<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$error=isset($error)?$error:'';
$title=isset($title)?$title:NAMA_PROGRAM;
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=$title?></title>

    <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?=BASE_URL?>assets/css/bootstrap.min.css">

	<link rel="stylesheet" href="<?=BASE_URL?>assets/css/main.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<header class="main-header">
    <!-- Logo -->
    <a href="<?=BASE_URL?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><?=$title?></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><?=$title?></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-building"></i> Perusahaan
            </a>
            <ul class="dropdown-menu">
              <li class="header">List Perusahaan</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">

                </ul>
              </li>
              <li class="footer"><a href="<?=BASE_URL?>perusahaan/listperusahaan">Lihat Semua Perusahaan</a></li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i>
              <span class="hidden-xs"></span>
            </a>
            <ul class="dropdown-menu">
              <!-- Menu Body -->
              <li class="user-body">

              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?=BASE_URL?>profil" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?=BASE_URL?>logout" class="btn btn-default btn-flat">Logout</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
		<div class="pull-left image">
          <i class="fa fa-user fa-big" style="color:white"></i>
        </div>
        <div class="pull-left info">
          <p></p>
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1></h1>
    </section>

    <!-- Main content -->
    <section class="content">
		<?= $contents ?>
	</section>
	<p class="footer" style="padding:5px 15px;margin:0">&copy; <?=date('Y')?> PT INDRACO All Right Reserved.</p>
</div>

<!-- jQuery 3 -->
<script src="<?=BASE_URL?>assets/js/jquery-3.5.0.min.js"></script>
<script src="<?=BASE_URL?>assets/js/bootstrap.min.js"></script>
<script src="<?=BASE_URL?>assets/js/main.js"></script>
</body>
</html>
