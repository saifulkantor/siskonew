<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$error=isset($error)?$error:'';
$title=isset($title)?$title:NAMA_PROGRAM;
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=$title?></title>

    <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?=BASE_URL?>assets/css/bootstrap.min.css">

	<link rel="stylesheet" href="<?=BASE_URL?>assets/css/main.css">
</head>
<body>
<header class="main-header">
    <!-- Logo -->
    <a href="<?=BASE_URL?>" class="logo">
      <img src="<?=BASE_URL?>assets/images/sisko-logo.png" style="height:50px;padding:12px" />
    </a>

  </header>


  <!-- Content Wrapper. Contains page content -->
<div class="container-fluid" style="background: #f3f4fa;">
  <div class="row">
    <?= $contents ?>
  </div>
	<p class="footer" style="padding:5px 15px;margin:0">&copy; <?=date('Y')?> PT INDRACO All Right Reserved.</p>
</div>

<!-- jQuery 3 -->
<script src="<?=BASE_URL?>assets/js/jquery-3.5.0.min.js"></script>
<script src="<?=BASE_URL?>assets/js/bootstrap.min.js"></script>
<script src="<?=BASE_URL?>assets/js/main.js"></script>
</body>
</html>
