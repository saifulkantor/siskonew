<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komoditas extends CI_Model {
	private $table = 'komoditas';
	private $id = 'id';

    function __construct()
	{
        parent::__construct();
	}

	public function get($id=0){
		$sql = "select a.*, b.jenis_perusahaan, b.icon,
				c.lokasi_perusahaan, c.bgcolor, c.color
				from {$this->table} a
				left join jenis_perusahaan b on a.id_jenis_perusahaan = b.id_jenis_perusahaan
				left join lokasi_perusahaan c on a.id_lokasi_perusahaan = c.id_lokasi_perusahaan
				where a.{$this->id} = ?";
		$query = $this->db->query($sql, $id);

		return $query->row();
	}

	function tampilantable($value,$forjs=false) {
		if ($forjs) {
			foreach ($value as $key => $val) {
				$value[$key]="'+value[\"$key\"]+'";
				if ($key=='harga') $value[$key]="'+value[\"$key\"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, \".\")+'";
			}
		}
		$data = '<div class="col-6 col-sm-4 col-md-3" style="padding-left:7px;padding-right:7px;"><div class="peritem" onclick="addtocart('.$value['id'].')">'.
		        '<img id="komimage'.$value['id'].'" src="'.BASE_URL.'assets/images/komoditas/'.$value['id'].'/image1.jpg" alt=" " class="img-responsive" style="width:100%">'.
		        '<p id="komnama'.$value['id'].'" class="namaproduk">'.$value["nama"].'</p>'.
		        '<p id="komharga'.$value['id'].'" class="hargaproduk">Rp. '.$value["harga"].'/'.$value["satuan"].'</p>'.
		        '<p id="komdeskripsi'.$value['id'].'" style="display:none">'.$value["deskripsi"].'</p>'.
		      '</div></div>';
		return $data;
	}

	function tampilankategori($value,$aktif=false) {
		$data = '<div class="perkategori'.$value['id'].' perkategori'.(($aktif)?' aktif':'').'"><div class="perkategoriitem" onclick="gantikategori(\''.$value['id'].'\')">'.
		        '<img id="komimage'.$value['id'].'" src="'.BASE_URL.'assets/images/komoditas/'.$value['id'].'/image1.jpg" alt=" " class="img-responsive" style="width:100%">'.
		        '<p id="komnama'.$value['id'].'" class="namaproduk">'.$value["nama"].'</p>'.
		      '</div></div>';
		return $data;
	}

	public function getAll(){
		$sql = "select a.*, b.jenis_perusahaan, b.icon,
				c.lokasi_perusahaan, c.bgcolor, c.color
				from {$this->table} a
				left join jenis_perusahaan b on a.id_jenis_perusahaan = b.id_jenis_perusahaan
				left join lokasi_perusahaan c on a.id_lokasi_perusahaan = c.id_lokasi_perusahaan
				order by a.urutan";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function getFilter ($filters=null,$lengkap=true,$page=1,$limit=25,$sql=false) {
      $query = 'select * FROM (SELECT k.*,IFNULL(CONVERT(SUBSTRING_INDEX(GROUP_CONCAT(kd.harga ORDER BY kd.tanggal DESC),",",1),UNSIGNED INTEGER),0) as harga,MAX(kd.tanggal) FROM komoditas k LEFT OUTER JOIN komoditas_data kd ON k.id=kd.id_komoditas';
      $urutkan='';
			unset($filters['page']);unset($filters['perpage']);
      if ($filters) {
        $query .= ' WHERE ';
        foreach ($filters as $key => $filter) {
        //$filter=htmlspecialchars($filter);
          if ($key=='nama')
            $query .= ' '.$key.' LIKE "%'.$filter.'%" AND';
          else if ($key=='tanggal')
            $query .= ' ('.$key.' <= "'.$filter.'" OR tanggal IS NULL) AND';
          else if ($key=='stok')
            $query .= ' '.$key.' <= '.$filter.' AND';
					else if ($key=='kat')
            $query .= ($filter!='semua')?' k.id_parent = '.$filter.' AND':' k.id_parent IS NOT NULL AND';
          else if ($key=='urutkan')
            $urutkan=($filter!='')?'ORDER BY '.$filter:'';
					else if ($key=='hargautuh')
            $query .=' harga <> 0 AND';
          else
            $query .= ($filter==NULL)?' '.$key.' IS NULL AND':' '.$key.'="'.$filter.'" AND';
        }
        $query=substr($query, 0, -3);
      } else {
        $query.=' WHERE k.id_parent IS NOT NULL ';
      }
      $query .= ' GROUP BY k.id, k.nama,k.deskripsi,k.satuan,k.urutan,k.id_parent,k.stok ORDER BY k.id_parent, IF(id_parent IS NULL,k.id,k.nama), kd.tanggal DESC) tbl ';
      if (!$lengkap) $query .= ' WHERE harga > 0 ';
      $query .= $urutkan;
      if ((int)$page>0 && (int)$limit>0) $query.=' LIMIT '.$limit.' OFFSET '.(($page-1)*$limit);
      if ($sql){
        return $query;
      } else {
        $datas = $this->db->query($query);
        return $datas->result_array();
      }
	}
	public function getMenu($id=0,$id_parent=null,$id_user=0){
		$id_parent=($id_parent!=null)?' = '.$id_parent:' IS NULL';
		$sql = "select a.nama_perusahaan, b.selesai, b.id_perusahaan_aplikasi, c.*, d.keyword, count(e.id_menu) AS anak
				from {$this->table} a
				left join perusahaan_menu b on a.id_perusahaan = b.id_perusahaan
				left join menu c on b.id_menu = c.id_menu
				left join user_menuauth d on b.id_menu = d.id_menu AND (d.id_user=".$id_user." OR id_user IS NULL)
				left join menu e on c.id_menu = e.id_parent OR e.id_menu IS NULL
				where a.{$this->id} = ".$id." AND c.id_parent ".$id_parent." GROUP BY c.id_menu order by c.id_menu";
		$query = $this->db->query($sql, [$id,$id_parent]);
		return $query->result();
	}

	function getData($number,$offset){
		$sql = "select a.*, b.jenis_perusahaan, b.icon,
				c.lokasi_perusahaan, c.bgcolor, c.color
				from {$this->table} a
				left join jenis_perusahaan b on a.id_jenis_perusahaan = b.id_jenis_perusahaan
				left join lokasi_perusahaan c on a.id_lokasi_perusahaan = c.id_lokasi_perusahaan
				LIMIT ".$number." OFFSET ".$offset;
		$query = $this->db->query($sql);
		return $query->result();
		// return $query = $this->db->get($this->table,$number,$offset)->result();
	}

	function updateone($datas=null,$wheres=null) {
		if ($datas!=null) {
			$query = 'UPDATE '.$this->table.' SET ';
			foreach ($datas as $key => $data) {
				if ($key!='id_perusahaan') $query .= ' '.$key.'="'.$data.'",';
			}
			$query=substr($query, 0, -1);
			if ($wheres!=null) {
				$query .= ' WHERE ';
				foreach ($wheres as $key => $where) {
					$query .= ' '.$key.'="'.$where.'" AND';
				}
				$query=substr($query, 0, -3);
			}
			$query.=';';

			// return $query;
			$datas = $this->db->query($query);
			return $datas;
		} else {
			return '';
		}
	}

	function createone($datas=null) {
		if ($datas!='') {
			$query = '(';
			$val = '(';
			foreach ($datas as $key => $data) {
				if ($key!='id_perusahaan') {
					$val .= $key.',';
					$query .= ($data==null)?'NULL,':'"'.$data.'",';
				}
			}
			$val=substr($val, 0, -1); $val.=')';
			$query=substr($query, 0, -1); $query.=');';
			$query = 'INSERT INTO '.$this->table.' '.$val.' VALUES '.$query;
			// return $query;
			$datas = $this->db->query($query);
			return $datas;
		}
	}

}
