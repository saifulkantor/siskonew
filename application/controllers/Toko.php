<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko extends MY_Controller {

	public function index()
	{
		$get = $this->input->get();
		$get['page'] = $get['page']??1;
		$get['perpage'] = $get['perpage']??24;
		$get['kat'] = $get['kat']??'semua';
		$get['hargautuh'] = '1';
		$this->load->model('Komoditas');
		$listkategori=$this->Komoditas->getFilter(['id_parent'=>NULL,'urutkan'=>'urutan asc'],true,1,25);
		// $listkomoditas=$this->Komoditas->getFilter($get,true,$get['page'],$get['perpage']);
		$this->setlayouts('toko','toko/index',['listkategori'=>$listkategori,'get'=>$get]);
	}
}
