<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function komoditas()
	{

		$get = $this->input->get();
		$post = $this->input->post();
		$page = $get['page']??1;
		$perpage = $get['perpage']??24;
		$csrf = $post['csrf']??'';
		$get['kat'] = $get['kat']??'semua';
		$get['hargautuh'] = '1';
		$this->load->model('Komoditas');
		if ($this->keamanan->generatecsrf()==$csrf) {
			echo json_encode($this->Komoditas->getFilter($get,true,$page,$perpage));
		} else {
			echo '';
		}
	}
	public function addtocart()
	{
		$this->load->model('Komoditas');
		$post = $this->input->post();
		$id = $post['id']??0;
		$id_user = $post['id_user']??0;
		$jumlah = $post['jumlah']??1;
		$csrf = $post['csrf']??'';
		if ($this->keamanan->generatecsrf()==$csrf) {
			$komoditas = $this->Komoditas->getFilter(['id'=>$id,'hargautuh'=>true],true,1,1);
			if ($komoditas) {
				$keranjang = $_SESSION['keranjang']??[];
				if (isset($keranjang[$id])) {
					if ($komoditas[0]['stok']>=$jumlah+$keranjang[$id]['jumlah']) {
						$_SESSION['keranjang'][$id]['jumlah']=$jumlah+$keranjang[$id]['jumlah'];
						if ($_SESSION['keranjang'][$id]['jumlah']<=0) unset($_SESSION['keranjang'][$id]);
						echo json_encode($_SESSION['keranjang']);
					} else {
						echo json_encode(['komoditas'=>'Jumlah Melebihi Stok']);
					}
				} else {
					$_SESSION['keranjang'][$id]=$komoditas[0];
					$_SESSION['keranjang'][$id]['jumlah']=$jumlah;
					if ($_SESSION['keranjang'][$id]['jumlah']<=0) unset($_SESSION['keranjang'][$id]);
					echo json_encode($_SESSION['keranjang']);
				}
			} else {
				echo json_encode(['komoditas'=>'Produk Tidak Ditemukan']);
			}
		} else {
			echo json_encode(['komoditas'=>'Gagal. Coba Refresh Halaman']);
		}
	}
}
